package edu.ib.springdata.dao;

import edu.ib.springdata.entity.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends CrudRepository<UserDto,String> {
    Optional<UserDto> findByName(String name);
}

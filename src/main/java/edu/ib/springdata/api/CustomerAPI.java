package edu.ib.springdata.api;

import edu.ib.springdata.entity.Customer;
import edu.ib.springdata.manager.CustomerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerAPI {

    private CustomerManager customers;

    @Autowired
    public CustomerAPI(CustomerManager customers) {
        this.customers = customers;
    }

    //GET
    @GetMapping("/customer/all")
    public Iterable<Customer> getAll() { return customers.findAllCustomer(); }

    @GetMapping("/customer")
    public Optional<Customer> getbyId(@RequestParam Long id) {
        return customers.findByIdCustomer(id);
    }

    //POST
    @PostMapping("/admin/customer")
    public Customer addCustomer(@RequestBody Customer newCustomer) {
        return customers.saveCustomer(newCustomer);
    }

    //PUT
    @PutMapping("/admin/customer")
    public Customer putCustomer(@RequestBody Customer newCustomer, @RequestParam Long id){
        return customers.editCustomer(newCustomer, id);
    }

    //PATCH
    @PatchMapping("/admin/customer")
    public Customer patchCustomer(@RequestBody Customer newCustomer, @RequestParam Long id){
        return customers.patchCustomer(newCustomer, id);
    }
}

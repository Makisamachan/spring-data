package edu.ib.springdata.api;

import edu.ib.springdata.entity.Product;
import edu.ib.springdata.manager.ProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductAPI {

    private ProductManager products;

    @Autowired
    public ProductAPI(ProductManager products) {
        this.products = products;
    }

    //GET
    @GetMapping("/product/all")
    public Iterable<Product> getAll() {
        return products.findAllProduct();
    }

    @GetMapping("/product")
    public Optional<Product> getById(@RequestParam Long id) {
        return products.findByIdProduct(id);
    }

    //POST
    @PostMapping("/admin/product")
    public Product addProduct(@RequestBody Product newProduct) {
        return products.saveProduct(newProduct);
    }

    //PUT
    @PutMapping("/admin/product")
    public Product putProduct(@RequestBody Product newProduct, @RequestParam Long id){
        return products.editProduct(newProduct, id);
    }

    //PATCH
    @PatchMapping("/admin/product")
    public Product patchProduct(@RequestBody Product newProduct, @RequestParam Long id){
        return products.patchProduct(newProduct, id);
    }
}

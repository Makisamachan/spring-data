package edu.ib.springdata.manager;

import edu.ib.springdata.PasswordEncoderConfig;
import edu.ib.springdata.entity.User;
import edu.ib.springdata.entity.UserDto;
import edu.ib.springdata.dao.UserRepo;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserManager {

    private UserRepo userRepo;
    private PasswordEncoderConfig passEnConfig;

    public UserManager(UserRepo userRepo, PasswordEncoderConfig passEnConfig) {
        this.userRepo = userRepo;
        this.passEnConfig = passEnConfig;
    }

    //GET
    public Optional<UserDto> findByName(String name) {
        return userRepo.findByName(name);
    }

    //POST
    public UserDto saveUser(UserDto user) {
        return userRepo.save(user);
    }

    //check
    public boolean checkUser(User user) {
        boolean userExists = false;

        if (findByName(user.getName()).isPresent()) {
            UserDto userDto = findByName(user.getName()).get();
            boolean encode = passEnConfig.passwordEncoder().matches(user.getPassword(), userDto.getPasswordHash());

            if (encode == true) {
                userExists = true;
            }
        }
        return userExists;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
//        saveUser(new UserDto("User","user","ROLE_CUSTOMER"));
//        saveUser(new UserDto("Admin","admin","ROLE_ADMIN"));
    }

}

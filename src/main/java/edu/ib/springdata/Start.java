package edu.ib.springdata;

import edu.ib.springdata.dao.ProductRepo;
import edu.ib.springdata.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Start {

    private ProductRepo productRepo;

    @Autowired
    public Start(ProductRepo productRepo){
        this.productRepo = productRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void runExample(){
//        Product product1 = new Product(4L, "Woda mineralna", 1.19f, true);
//        productRepo.save(product1);
//
//        Product product2 = new Product(5L,"Proszek do prania", 29.99f, true);
//        productRepo.save(product2);
//
//        Product product3 = new Product(6L,"Papier toaletowy", 16.99f, false);
//        productRepo.save(product3);
//
//        Product product4 = new Product(7L,"Mydło w płynie", 7.39f, false);
//        productRepo.save(product4);
    }
}

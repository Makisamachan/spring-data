package edu.ib.springdata;

import edu.ib.springdata.dao.CustomerRepo;
import edu.ib.springdata.dao.OrderRepo;
import edu.ib.springdata.dao.ProductRepo;
import edu.ib.springdata.entity.Customer;
import edu.ib.springdata.entity.Order;
import edu.ib.springdata.entity.Product;
import edu.ib.springdata.entity.User;
import edu.ib.springdata.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {

    private ProductRepo productRepo;
    private OrderRepo orderRepo;
    private CustomerRepo customerRepo;
    private UserManager userManager;
    private UserDtoBuilder userDtoBuilder;

    @Autowired
    public DbMockData(ProductRepo productRepo, OrderRepo orderRepo, CustomerRepo customerRepo, UserManager userManager, UserDtoBuilder userDtoBuilder) {
        this.productRepo = productRepo;
        this.orderRepo = orderRepo;
        this.customerRepo = customerRepo;
        this.userManager = userManager;
        this.userDtoBuilder = userDtoBuilder;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill(){
        Product product1 = new Product(1L,"Korek", 2.55f, true);
        Product product2 = new Product(2L,"Rura", 5f, true);
        Customer customer = new Customer(1L, "Jan Kowalski", "Warszawa");
        Set<Product> products = new HashSet<Product>(){
            {
            add(product1);
            add(product2);
        }};

        Order order = new Order(1L, customer, products, LocalDateTime.now(), "in progress");

        User admin = new User("Admin1", "admin1", "ROLE_ADMIN");
        User user = new User("User1", "user1", "ROLE_CUSTOMER");

        productRepo.save(product1);
        productRepo.save(product2);
        customerRepo.save(customer);
        orderRepo.save(order);

        userManager.saveUser(userDtoBuilder.createUser(admin));
        userManager.saveUser(userDtoBuilder.createUser(user));
    }
}

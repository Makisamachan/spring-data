package edu.ib.springdata;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/h2**","/api/getToken","/api/product/**","/api/order/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("api/customer/**").hasRole("CUSTOMER")
                .antMatchers("api/admin/**").hasRole("ADMIN")
                .and()
                .httpBasic().and()
                .csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .addFilter(new JwtFilter(authenticationManager()));
    }
}

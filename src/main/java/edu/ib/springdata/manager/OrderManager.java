package edu.ib.springdata.manager;


import edu.ib.springdata.dao.OrderRepo;
import edu.ib.springdata.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderManager {

    private OrderRepo orderRepo;

    @Autowired
    public OrderManager(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }

    //GET
    public Optional<Order> findByIdOrder(Long id) {
        return orderRepo.findById(id);
    }

    public Iterable<Order> findAllOrder() {
        return orderRepo.findAll();
    }

    //POST
    public Order saveOrder(Order order) {
        return orderRepo.save(order);
    }

    //PUT
    public Order editOrder(Order newOrder, Long id) {
        return orderRepo.findById(id)
                .map(order -> {
                    order.setCustomer(newOrder.getCustomer());
                    order.setProduct(newOrder.getProduct());
                    order.setPlaceDate(newOrder.getPlaceDate());
                    order.setStatus(newOrder.getStatus());
                    return orderRepo.save(order);
                })
                .orElseGet(() -> {
                    newOrder.setId(id);
                    return orderRepo.save(newOrder);
                });
    }

    //PATCH
    public Order patchOrder(Order newOrder, Long id) {
        return orderRepo.findById(id)
                .map(order -> {
                    order.setCustomer(newOrder.getCustomer());
                    order.setProduct(newOrder.getProduct());
                    order.setPlaceDate(newOrder.getPlaceDate());
                    order.setStatus(newOrder.getStatus());
                    return orderRepo.save(order);
                })
                .orElseGet(() -> {
                    newOrder.setId(id);
                    return orderRepo.save(newOrder);
                });
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){

    }
}

package edu.ib.springdata.manager;

import edu.ib.springdata.dao.CustomerRepo;
import edu.ib.springdata.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerManager {

    private CustomerRepo customerRepo;

    @Autowired
    public CustomerManager(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    //GET
    public Optional<Customer> findByIdCustomer(Long id) {
        return customerRepo.findById(id);
    }

    public Iterable<Customer> findAllCustomer() {
        return customerRepo.findAll();
    }

    //POST
    public Customer saveCustomer(Customer customer) {
        return customerRepo.save(customer);
    }

    //PUT
    public Customer editCustomer(Customer newCustomer, Long id) {
        return customerRepo.findById(id)
                .map(customer -> {
                    customer.setName(newCustomer.getName());
                    customer.setAddress(newCustomer.getAddress());
                    return customerRepo.save(customer);
                })
                .orElseGet(() -> {
                    newCustomer.setId(id);
                    return customerRepo.save(newCustomer);
                });
    }

    //PATCH
    public Customer patchCustomer(Customer newCustomer, Long id) {
        return customerRepo.findById(id)
                .map(customer -> {
                    customer.setName(newCustomer.getName());
                    customer.setAddress(newCustomer.getAddress());
                    return customerRepo.save(customer);
                })
                .orElseGet(() -> {
                    newCustomer.setId(id);
                    return customerRepo.save(newCustomer);
                });
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        saveCustomer(new Customer(2L, "Marian Marianowski", "Wrocław"));
        saveCustomer(new Customer(3L, "Anna Annowska", "Warszawa"));
    }
}

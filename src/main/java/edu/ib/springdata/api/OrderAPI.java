package edu.ib.springdata.api;

import edu.ib.springdata.entity.Order;
import edu.ib.springdata.manager.OrderManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderAPI {

    private OrderManager orders;

    @Autowired
    public OrderAPI(OrderManager orders) {
        this.orders = orders;
    }

    //GET
    @GetMapping("/order/all")
    public Iterable<Order> getAll(){
        return orders.findAllOrder();
    }

    @GetMapping("/order")
    public Optional<Order> getById(@RequestParam Long id) {
        return orders.findByIdOrder(id);
    }

    //POST
    @PostMapping("/order")
    public Order addOrder(@RequestBody Order newOrder) {
        return orders.saveOrder(newOrder);
    }

    //PUT
    @PutMapping("/admin/order")
    public Order putOrder(@RequestBody Order newOrder, @RequestParam Long id){
        return orders.editOrder(newOrder, id);
    }

    //PATCH
    @PatchMapping("/admin/order")
    public Order patchOrder(@RequestBody Order newOrder, @RequestParam Long id){
        return orders.patchOrder(newOrder, id);
    }
}

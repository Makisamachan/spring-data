package edu.ib.springdata.api;

import edu.ib.springdata.entity.User;
import edu.ib.springdata.manager.UserManager;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;

@RestController
@RequestMapping("/api")
public class LoginAPI {
    private UserManager userManager;

    public LoginAPI(UserManager userManager) {
        this.userManager = userManager;
    }

    @PostMapping("/getToken")
    public String getToken(@RequestBody User user) throws Exception {
        long currTime = System.currentTimeMillis();
        //long currTime = System.currentTimeMillis();

        LocalDateTime start = LocalDateTime.now();
        LocalDateTime stop = LocalDateTime.now().plusMinutes(15);

        if (userManager.checkUser(user)) {
            return Jwts.builder()
                    .setSubject(user.getName())
                    .claim("role", user.getRole())
                    .setIssuedAt(java.util.Date.from(start.atZone(ZoneId.systemDefault()).toInstant()))
                    .setExpiration(java.util.Date.from(stop.atZone(ZoneId.systemDefault()).toInstant()))
                    .signWith(SignatureAlgorithm.HS512, user.getPassword().getBytes("UTF-8"))
                    .setHeaderParam("typ","JWT")
                    .compact();

        } else {
            throw new Exception("User not found");
        }
    }
}

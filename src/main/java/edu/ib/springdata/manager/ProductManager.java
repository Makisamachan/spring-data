package edu.ib.springdata.manager;

import edu.ib.springdata.dao.ProductRepo;
import edu.ib.springdata.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductManager {

    private ProductRepo productRepo;

    @Autowired
    public ProductManager(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    //GET
    public Optional<Product> findByIdProduct(Long id) {
        return productRepo.findById(id);
    }

    public Iterable<Product> findAllProduct() {
        return productRepo.findAll();
    }

    //POST
    public Product saveProduct(Product product) {
        return productRepo.save(product);
    }

    //PUT
    public Product editProduct(Product newProduct, Long id) {
        return productRepo.findById(id)
                .map(product -> {
                    product.setName(newProduct.getName());
                    product.setPrice(newProduct.getPrice());
                    product.setAvailble(newProduct.isAvailble());
                    return productRepo.save(product);
                })
                .orElseGet(() -> {
                    newProduct.setId(id);
                    return productRepo.save(newProduct);
                });
    }

    //PATCH
    public Product patchProduct(Product newProduct, Long id) {
        return productRepo.findById(id)
                .map(product -> {
                    product.setName(newProduct.getName());
                    product.setPrice(newProduct.getPrice());
                    product.setAvailble(newProduct.isAvailble());
                    return productRepo.save(product);
                })
                .orElseGet(() -> {
                    newProduct.setId(id);
                    return productRepo.save(newProduct);
                });
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        saveProduct(new Product(3L,"Woda",1.19f,true));
        saveProduct(new Product(4L,"Papier",24.45f,true));
    }
}

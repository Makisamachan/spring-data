package edu.ib.springdata;

import edu.ib.springdata.entity.User;
import edu.ib.springdata.entity.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserDtoBuilder {

    private PasswordEncoderConfig passwordEncoderConfig;

    public UserDtoBuilder(PasswordEncoderConfig passwordEncoderConfig) {
        this.passwordEncoderConfig = passwordEncoderConfig;
    }

    public UserDto createUser(User user) {
        return new UserDto(user.getName(), passwordEncoderConfig.passwordEncoder().encode(user.getPassword()), user.getRole());
    }
}
